// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");
const {ethers, upgrades} = require("hardhat");

async function main() {
  await hre.run('compile');
  const [
    royalGroup,
    airdrop,
    swap,
    internalSwap,
    friends,
    private_0,
    private_1,
    private_2,
    ido,
  ] = [
    "0x251ac881635A902145dB3F4b09a7283c1F1Eed5D",
    "0x988736DfeE1e02C373255Bbac7BbB8A2736Fcb25",
    "0x898AFffC3d72bC98dcA7070DA83872AC79dC160A",
    "0xD2169B0e9E01b56679f19Be74269B35bFeEd77B9",
    "0x2Fc17da644081360cE8f6C4600ACd3a581352103",
    "0x92ae445e1CaF182E6E15f2067982766559dfeCd2",
    "0x23eb7f36e72D7c022126adFD304e3AE1D39A5b2a",
    "0x221d4adb070F5bdC0089a2DE0DF0E07fbe1310E9",
    "0xa731c377D66b04Ae242D13e2791585226068AFf9"
  ];

  console.log("Deploying GarudaEleven Token");

  // We get the contract to deploy
  const GarudaEleven = await hre.ethers.getContractFactory("GarudaElevenToken");
  const garudaEleven = await hre.upgrades.deployProxy(GarudaEleven, [], {kind: 'uups'});

  await garudaEleven.deployed();

  console.log("GarudaEleven deployed to:", garudaEleven.address);

  console.log("Deploying RoyalVestingWallet");
  const RoyalVestingWallet = await ethers.getContractFactory("RoyalVestingWallet");
  const royalVestingWallet = await RoyalVestingWallet.deploy(
    royalGroup,
    Math.floor(new Date().getTime() / 1000),
    1 * 30 * 24 * 60 * 60, // 1 month
  );

  await royalVestingWallet.deployed();
  console.log("RoyalVestingWallet deployed to:", royalVestingWallet.address);

  await garudaEleven.transfer(royalVestingWallet.address, ethers.utils.parseEther('30000000'));
  await garudaEleven.transfer(airdrop, ethers.utils.parseEther('4000000'));
  await garudaEleven.transfer(swap, ethers.utils.parseEther('10000000'));
  await garudaEleven.transfer(internalSwap, ethers.utils.parseEther('5000000'));
  await garudaEleven.transfer(friends, ethers.utils.parseEther('5000000'));
  await garudaEleven.transfer(private_0, ethers.utils.parseEther('5000000'));
  await garudaEleven.transfer(private_1, ethers.utils.parseEther('10000000'));
  await garudaEleven.transfer(private_2, ethers.utils.parseEther('15000000'));
  await garudaEleven.transfer(ido, ethers.utils.parseEther('16000000'));

  console.log("Transfer complete");
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
