// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "./GarudaElevenToken.sol";

/// @custom:security-contact info@empatnusabangsa.com
contract GarudaElevenTokenTestUpgrade is GarudaElevenToken {
    function testMethodUpgrade() pure external returns (string memory) {
        return "testMethodUpgrade";
    }
}
