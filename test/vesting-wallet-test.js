const {expect} = require("chai");
const {ethers, upgrades, waffle} = require("hardhat");
const hre = require("hardhat");
const {startOfDay, addDays} = require("date-fns");



describe("RoyalVestingWallet", function () {
  it("can be deployed", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    // const Greeter = await ethers.getContractFactory("GarudaElevenToken");
    // const greeter = await Greeter.deploy( "Hello, world!");
    // await greeter.deployed();

    const startTime = startOfDay(new Date());

    const RoyalVestingWallet = await ethers.getContractFactory("RoyalVestingWallet");
    const royalVestingWallet = await RoyalVestingWallet.deploy(
      addr1.address,
      Math.floor(startTime.getTime() / 1000),
      1 * 30 * 24 * 60 * 60, // 1 month
    );

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    const threeMillionToken = ethers.BigNumber.from(30).mul(ethers.BigNumber.from(10).pow(6)).mul(ethers.BigNumber.from(10).pow(18));

    await garudaElevenToken.transfer(royalVestingWallet.address, threeMillionToken);
    expect(await garudaElevenToken.balanceOf(royalVestingWallet.address)).to.equal(threeMillionToken);
  });

  it("can release third of vested amount in 10 days", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    // const Greeter = await ethers.getContractFactory("GarudaElevenToken");
    // const greeter = await Greeter.deploy( "Hello, world!");
    // await greeter.deployed();

    const startTime = startOfDay(new Date());

    const RoyalVestingWallet = await ethers.getContractFactory("RoyalVestingWallet");
    const royalVestingWallet = await RoyalVestingWallet.deploy(
      addr1.address,
      Math.floor(startTime.getTime() / 1000),
      1 * 30 * 24 * 60 * 60, // 1 month
    );

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    const threeMillionToken = ethers.BigNumber.from(30).mul(ethers.BigNumber.from(10).pow(6)).mul(ethers.BigNumber.from(10).pow(18));

    await garudaElevenToken.transfer(royalVestingWallet.address, threeMillionToken);
    expect(await garudaElevenToken.balanceOf(royalVestingWallet.address)).to.equal(threeMillionToken);

    const vestingWalletBalanceBefore = (await garudaElevenToken.balanceOf(royalVestingWallet.address));
    const userWalletBalanceBefore = (await garudaElevenToken.balanceOf(addr1.address));

    // half of eth can be withdrawn
    await ethers.provider.send('evm_setNextBlockTimestamp', [addDays(startTime, 10).getTime() / 1000]);
    await ethers.provider.send('evm_mine');

    await royalVestingWallet['release(address)'](garudaElevenToken.address);

    const vestingWalletBalanceAfter = (await garudaElevenToken.balanceOf(royalVestingWallet.address));
    const userWalletBalanceAfter = (await garudaElevenToken.balanceOf(addr1.address));

    expect(vestingWalletBalanceBefore.sub(vestingWalletBalanceAfter)).to.equal(userWalletBalanceAfter);
    expect(vestingWalletBalanceBefore).to.equal(threeMillionToken);
    expect(userWalletBalanceBefore).to.equal(0);
    expect(userWalletBalanceAfter.mul(100).div(threeMillionToken)).to.equal(33); // 33% of the token can be retrieved
  });

  it("can release all vested amount in 30 days", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const startTime = startOfDay(new Date());

    const RoyalVestingWallet = await ethers.getContractFactory("RoyalVestingWallet");
    const royalVestingWallet = await RoyalVestingWallet.deploy(
      addr1.address,
      Math.floor(startTime.getTime() / 1000),
      1 * 30 * 24 * 60 * 60, // 1 month
    );

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    const threeMillionToken = ethers.BigNumber.from(30).mul(ethers.BigNumber.from(10).pow(6)).mul(ethers.BigNumber.from(10).pow(18));

    await garudaElevenToken.transfer(royalVestingWallet.address, threeMillionToken);
    expect(await garudaElevenToken.balanceOf(royalVestingWallet.address)).to.equal(threeMillionToken);

    const vestingWalletBalanceBefore = (await garudaElevenToken.balanceOf(royalVestingWallet.address));
    const userWalletBalanceBefore = (await garudaElevenToken.balanceOf(addr1.address));

    await ethers.provider.send('evm_setNextBlockTimestamp', [addDays(startTime, 30).getTime() / 1000]);
    await ethers.provider.send('evm_mine');

    await royalVestingWallet['release(address)'](garudaElevenToken.address);

    const vestingWalletBalanceAfter = (await garudaElevenToken.balanceOf(royalVestingWallet.address));
    const userWalletBalanceAfter = (await garudaElevenToken.balanceOf(addr1.address));

    expect(vestingWalletBalanceBefore.sub(vestingWalletBalanceAfter)).to.equal(userWalletBalanceAfter);
    expect(vestingWalletBalanceBefore).to.equal(threeMillionToken);
    expect(userWalletBalanceBefore).to.equal(0);
    expect(vestingWalletBalanceAfter).to.equal(0);
    expect(userWalletBalanceAfter).to.equal(threeMillionToken);
  });
});
