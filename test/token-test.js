const {expect} = require("chai");
const {ethers, upgrades, waffle} = require("hardhat");
const hre = require("hardhat");


describe("GarudaElevenToken", function () {
  it("can be deployed", async function () {
    // const Greeter = await ethers.getContractFactory("GarudaElevenToken");
    // const greeter = await Greeter.deploy("Hello, world!");
    // await greeter.deployed();

    const GarudaEleven = await ethers.getContractFactory("GarudaElevenToken");
    const garudaEleven = await upgrades.deployProxy(GarudaEleven, [], {kind: 'uups'});

    await garudaEleven.deployed();

    // expect(await garudaEleven.greet()).to.equal("Hello, world!");
    //
    // const setGreetingTx = await garudaEleven.setGreeting("Hola, mundo!");
    //
    // // wait until the transaction is mined
    // await setGreetingTx.wait();
    //
    // expect(await garudaEleven.greet()).to.equal("Hola, mundo!");
  });

  it("assigns initial balance", async function () {
    const [owner] = await ethers.getSigners();

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    await garudaElevenToken.deployed();

    const ownerBalance = await garudaElevenToken.balanceOf(owner.address);
    expect(ownerBalance).to.equal(await garudaElevenToken.totalSupply());
  });

  it("Should transfer tokens between accounts", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    await garudaElevenToken.deployed();

    const token50 = ethers.BigNumber.from(50).mul(ethers.BigNumber.from(10).pow(18));

    // Transfer 50 tokens from owner to addr1
    await garudaElevenToken.transfer(addr1.address, token50);
    expect(await garudaElevenToken.balanceOf(addr1.address)).to.equal(token50);

    // Transfer 50 tokens from addr1 to addr2
    await garudaElevenToken.connect(addr1).transfer(addr2.address, token50);
    expect(await garudaElevenToken.balanceOf(addr2.address)).to.equal(token50);
  });

  it("can pause and unpause the contract", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    await garudaElevenToken.deployed();


    const token50 = ethers.BigNumber.from(50).mul(ethers.BigNumber.from(10).pow(18));

    // Transfer 50 tokens from owner to addr1
    await garudaElevenToken.transfer(addr1.address, token50);
    expect(await garudaElevenToken.balanceOf(addr1.address)).to.equal(token50);

    const pauseTx = await garudaElevenToken.pause();

    await pauseTx.wait();

    await expect(
      garudaElevenToken.transfer(addr2.address, token50)
    ).to.be.revertedWith("Pausable: paused");

    const unpauseTx = await garudaElevenToken.unpause();

    await unpauseTx.wait();

    await garudaElevenToken.transfer(addr1.address, token50);
    expect(await garudaElevenToken.balanceOf(addr1.address)).to.equal(token50.mul(2));
  });

  it(`cant transfer if no balance avaiable`, async function() {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const token50 = ethers.BigNumber.from(50).mul(ethers.BigNumber.from(10).pow(18));

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    await garudaElevenToken.deployed();

    await expect(
      garudaElevenToken.connect(addr1).transfer(addr2.address, token50)
    ).to.be.revertedWith("ERC20: transfer amount exceeds balance");
  });

  it(`can be upgraded`, async function() {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const GarudaElevenToken = await ethers.getContractFactory("GarudaElevenToken");
    const garudaElevenToken = await upgrades.deployProxy(GarudaElevenToken, [], {kind: 'uups'});

    await garudaElevenToken.deployed();

    const GarudaElevenTokenTestUpgrade = await ethers.getContractFactory("GarudaElevenTokenTestUpgrade");

    const garudaElevenTokenUpgraded = await upgrades.upgradeProxy(garudaElevenToken.address, GarudaElevenTokenTestUpgrade);

    expect(await garudaElevenTokenUpgraded.testMethodUpgrade()).to.equal("testMethodUpgrade");
  });
});
